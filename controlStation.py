#!/usr/bin/python
import socket
import time
import sys
import datetime
import subprocess
import shutil
import json

path = "/var/www/erlenddahl.net/html/pi/data/info.json" #Must have trailing slash!
subnet = "192.168.1.*"
ignoreComputers = ["192.168.1.119", "192.168.1.1"]

def tcp_connect( HostIp, Port ):
    global s
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(10)
    s.connect((HostIp, Port))
    return
   
def tcp_write(D):
    s.send(D + '\r')
   
def tcp_read( ):
    b = ''
    while True:
        try:
            a = s.recv(1)
            if a == "\r":
                break
        except:
            break
        b = b + a
    return b

def tcp_close( ):
   s.close()

def send_command(ip, cmd):
    tcp_connect( ip, 15905 )
    tcp_write(cmd)
    data = tcp_read().strip()
    tcp_close();
    return data

def get_computers():
    global subnet
    s = s = subprocess.check_output(["nmap", "-sP", subnet])
    return [y.split("(")[1].replace(")","") if "(" in y else y for y in [x.split(" for ")[1] for x in s.split('\n') if " for " in x]]

def scan_info():
    global path
    global ignoreComputers

    try:
        with open(path) as data_file:    
            results = json.load(data_file)
    except:
        results = {}
        results["pis"] = {}
    print results
    for ip in get_computers():
        if ip in ignoreComputers: continue
        try:
            print "Querying '" + ip + "'"
            info = send_command(ip, "info")
            j = json.loads(info)
            j['timestamp'] = int(round(time.time() * 1000))
            results["pis"][j["serial"]] = j
            print "    > Retrieved data for " + ip
        except Exception as e:
            print "    > Failure fetching info from", ip, e
            pass


    f = open(path,'w')
    f.write(json.dumps(results))
    f.close()
    print "Wrote data to json file."

if len(sys.argv) > 2:
    print sys.argv
    print send_command(sys.argv[1], ";".join(sys.argv[2:]))
else:
    scan_info()