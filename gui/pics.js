angular.module('picsApp', [])

    .controller("LoginController", function($http, $scope){

        var controller = this;

        controller.login = function(){
            $http.post('login.php', {un: $scope.username, pw: $scope.password}, {}).then(
                function successCallback(response) {
                    if(response.data==="ok"){
                        $scope.message="You are logged in. Redirecting ... (Or click <a href='index.php'>here</a>.)";
                        window.location = "index.php";
                    }else{
                        $scope.message="Invalid username and/or password.";
                    }
                }, function errorCallback(response) {
                    $scope.message="Something went wrong. Please try again.";
                });
        }
    })

    .controller('ListController', function($http) {
        var sys = this;
        sys.onlinePis = {};
        sys.offlinePis = {};
        sys.allPis = {};

        sys.getURLParameter = function(name) {
            return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
        }

        sys.initGraphs = function(){
            var gaugeOptions = {
                chart: {
                    type: 'solidgauge',
                    backgroundColor: null, 
                    plotBackgroundColor: null, 
                    plotBorderWidth: 0, 
                    plotShadow: false
                },
                title: null,
                pane: {
                    center: ['50%', '85%'],
                    size: '100%',
                    startAngle: -90,
                    endAngle: 90,
                    background: {
                        backgroundColor: '#EEE',
                        innerRadius: '60%',
                        outerRadius: '100%',
                        shape: 'arc'
                    }
                },
                tooltip: {
                    enabled: false
                },
                yAxis: {
                    min: 0,
                    stops: [
                        [0.1, '#55BF3B'], // green
                        [0.5, '#DDDF0D'], // yellow
                        [0.9, '#DF5353'] // red
                    ],
                    lineWidth: 0,
                    minorTickInterval: null,
                    tickPixelInterval: 400,
                    tickWidth: 0,
                    title: {
                        y: -70
                    },
                    labels: {
                        y: 16
                    }
                },
                credits: false,
                plotOptions: {
                    solidgauge: {
                        dataLabels: {
                            y: 5,
                            borderWidth: 0,
                            useHTML: true
                        }
                    }
                }
            };

            $('#chartDiskSpace').highcharts(Highcharts.merge(gaugeOptions, {
                yAxis: {
                    max: 100,
                    title: {
                        text: 'Hard disk'
                    },
                    tickPositions: [0, 100]
                },
                series: [{
                    name: 'Hard disk',
                    data: [0],
                    dataLabels: {
                        format: '<div style="text-align:center"><span style="font-size:17px;color:black">{y}</span>' +
                               '<span style="font-size:12px;color:#555">MB</span></div>'
                    },
                    tooltip: {
                        valueSuffix: ' MB'
                    }
                }]

            }));

            $('#chartMemory').highcharts(Highcharts.merge(gaugeOptions, {
                yAxis: {
                    max: 100,
                    title: {
                        text: 'Memory'
                    },
                    tickPositions: [0, 100]
                },
                series: [{
                    name: 'Hard disk',
                    data: [0],
                    dataLabels: {
                        format: '<div style="text-align:center"><span style="font-size:17px;color:black">{y}</span>' +
                               '<span style="font-size:12px;color:#555">MB</span></div>'
                    },
                    tooltip: {
                        valueSuffix: ' MB'
                    }
                }]

            }));

            $('#chartCPU').highcharts(Highcharts.merge(gaugeOptions, {
                yAxis: {
                    max: 100,
                    title: {
                        text: 'CPU 5 min'
                    },
                    tickPositions: [0, 100]
                },
                series: [{
                    name: 'CPU 5 min',
                    data: [0],
                    dataLabels: {
                        format: '<div style="text-align:center"><span style="font-size:17px;color:black">{y}</span>' +
                               '<span style="font-size:12px;color:#555">%</span></div>'
                    },
                    tooltip: {
                        valueSuffix: ' %'
                    }
                }]

            }));
        };

        sys.select = function(item){
            sys.selected = item;

            sys.updateGraphs();
        };

        sys.updateGraphs = function(){
            if(!$('#chartDiskSpace').highcharts()) sys.initGraphs();

            var chartHd = $('#chartDiskSpace').highcharts();
            var chartMem = $('#chartMemory').highcharts();
            var chartCpu = $('#chartCPU').highcharts();

            var item = sys.selected;

            var valHd = item ? item["disk used"] : 0;
            var maxHd = item ? item["disk total"] : 100;
            var valMem = item ? (item["ram total"] - item["ram available"]) : 0;
            var maxMem = item ? item["ram total"] : 100;
            var valCpu = item ? Math.round(item["cpu 5min lavg"]*100) : 0;
            
            if(chartHd){
                chartHd.series[0].points[0].update(valHd);
                chartHd.yAxis[0].update({
                    max: maxHd,
                    tickPositions: [0, maxHd]
                });
            }
            if(chartMem){
                chartMem.series[0].points[0].update(valMem);
                chartMem.yAxis[0].update({
                    max: maxMem,
                    tickPositions: [0, maxMem]
                });
            }
            if(chartCpu){
                chartCpu.series[0].points[0].update(valCpu);
            }
        }

        sys.updateSelected = function(){
            if(!sys.selected) return;
            for(piid in sys.onlinePis){
                var pi = sys.onlinePis[piid];
                if(pi && pi["serial"] === sys.selected["serial"]){
                    sys.select(pi);
                    return;
                }
            }
            for(piid in sys.offlinePis){
                var pi = sys.offlinePis[piid];
                if(pi && pi["serial"] === sys.selected["serial"]){
                    sys.select(pi);
                    return;
                }
            }
        };

        sys.selectByHostname = function(hostname){
            for(piid in sys.onlinePis){
                var pi = sys.onlinePis[piid];
                if(pi && pi["hostname"] === hostname){
                    sys.select(pi);
                    return;
                }
            }
            for(piid in sys.offlinePis){
                var pi = sys.offlinePis[piid];
                if(pi && pi["hostname"] === hostname){
                    sys.select(pi);
                    return;
                }
            }
        };

        sys.isSelected = function(item){
            return sys.selected && sys.selected["serial"] === item["serial"];
        };

        sys.readUrl = function(){
            var single = sys.getURLParameter("single");
            if(single){
                sys.selectByHostname(single);
                sys.hideLists = true;
            }
            var all = sys.getURLParameter("all");
            if(all){
                sys.selected = null;
                sys.hideLists = true;
                sys.showAll = true;
            }
        }

        sys.getData = function(){
            $http.get('data.php').
                then(function(response) {
                    sys.json = response.data;
                    for(piid in sys.json.pis){
                        var pi = sys.json.pis[piid];
                        pi.time = moment(pi.timestamp).calendar()
                        pi.online = moment().diff(moment(pi.timestamp), "minutes") <= 3

                        var attributes = [];
                        for(attr in pi){
                            attributes.push(attr);
                        }
                        pi.attributes = attributes;

                        pi.ips = [];
                        if(pi["ip eth0"] && pi["ip eth0"] != "0.0.0.0")
                            pi.ips.push(pi["ip eth0"]);
                        if(pi["ip wlan0"] && pi["ip wlan0"] != "0.0.0.0"){
                            pi.ips.push(pi["ip wlan0"]);
                        }

                        if(pi.online){
                            sys.onlinePis[piid] = pi;
                            delete sys.offlinePis[piid];
                        }
                        else{
                            delete sys.onlinePis[piid];
                            sys.offlinePis[piid] = pi;
                        }
                        sys.allPis[piid] = pi;
                    };
                    sys.updateSelected();
                    sys.readUrl();
                }, function(response) {
                    console.log(response);
                });
        };

        moment.locale("nb");
        sys.initGraphs();

        (function update()
        {
            sys.getData();

            setTimeout(function() {
                update();
            }, 10000);
        })();
    });

